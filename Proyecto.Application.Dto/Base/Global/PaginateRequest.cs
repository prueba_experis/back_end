﻿
namespace Proyecto.Application.Dto.Base.Products
{
    public class PaginateRequest
    {
        public int cantPage { get; set; }
        public int numberPage { get; set; }
    }
}
