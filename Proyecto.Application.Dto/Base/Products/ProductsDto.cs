﻿
namespace Proyecto.Application.Dto.Base.Products
{
    public class ProductsDto
    {
        public int id { get; set; }
        public string? name { get; set; }
        public decimal? price { get; set; }

    }
}
