﻿
namespace Proyecto.Application.Dto.Base.Products
{
    public class ProductsRequestDto
    {
        public string filter { get; set; }
        public PaginateRequest dataPage { get; set; }

    }
}
