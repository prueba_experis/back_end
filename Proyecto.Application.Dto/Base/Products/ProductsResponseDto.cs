﻿using System;
using System.Collections.Generic;

namespace Proyecto.Application.Dto.Base.Products
{
    public class ProductsResponseDto
    {
        public int success { get; set; }
        public int maxPage { get; set; }
        public string? menssage { get; set; }
        public List<ProductsDto>? listData { get; set; }
    }
}
