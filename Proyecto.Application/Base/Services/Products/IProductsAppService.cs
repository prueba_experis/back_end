﻿using System.Threading.Tasks;
using Proyecto.Application.Dto.Base.Products;


namespace Proyecto.Application.Base.Services.Products
{
    public interface IProductsAppService
    {
        Task<ProductsResponseDto> GetProductsAsync(ProductsRequestDto request);
    }
}
