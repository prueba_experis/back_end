﻿using System;
using Proyecto.Domain.Base;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using Proyecto.Application.Dto.Base.Products;

namespace Proyecto.Application.Base.Services.Products
{
    public class ProductsAppService : IProductsAppService
    {
        private readonly IProductsRepository _IProductsRepository;
        public IConfiguration _iConfiguration { get; }

        public ProductsAppService(
            IProductsRepository iProductsRepository,
            IConfiguration iConfiguration
            )
        {
            _IProductsRepository = iProductsRepository;
            _iConfiguration = iConfiguration;
        }


        public async Task<ProductsResponseDto> GetProductsAsync(ProductsRequestDto permission)
        {
            ProductsResponseDto res = new ProductsResponseDto();
            try
            {
               res = await _IProductsRepository.GetProductsAsync(permission);
            }
            catch (Exception e)
            {
                res.success = 0;
                res.menssage = e.Message;
            }
            
            return res;
        }
    }
}
