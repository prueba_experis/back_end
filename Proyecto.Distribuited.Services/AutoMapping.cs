﻿using AutoMapper;
using Proyecto.Application.Dto.Base.Products;
using Proyecto.Domain.Entities.Base.Models;

namespace Proyecto.Distribuited.Services
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {
            //Usuario
            CreateMap<ProductModel, ProductsDto>()
                .ForMember(d => d.id, opt => opt.MapFrom(src => src.IDPRODUCT))
                .ForMember(d => d.name, opt => opt.MapFrom(src => src.NAME))
                .ForMember(d => d.price, opt => opt.MapFrom(src => src.PRICE));
        }
    }
}
