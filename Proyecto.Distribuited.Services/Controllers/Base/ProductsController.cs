﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Proyecto.Application.Base.Services.Products;
using Proyecto.Application.Dto.Base.Products;

namespace Proyecto.Distribuited.Services.Controllers.Base
{
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : Controller
    {
        private readonly IProductsAppService _IProductsAppService;

        public ProductsController(IProductsAppService IProductsAppService)
        {
            _IProductsAppService = IProductsAppService;
        }

        [HttpPost("GetProducts")]
        public async Task<IActionResult> GetProducts([FromBody] ProductsRequestDto request)
        {
            var respuest = await _IProductsAppService.GetProductsAsync(request);
            return StatusCode(200, respuest);
        }

    }
}
