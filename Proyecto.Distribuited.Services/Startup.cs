using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Proyecto.Application.Base.Services.Products;
using Proyecto.Domain.Base;
using Proyecto.Infraestructure.Data.Base.Contexts;
using Proyecto.Infraestructure.Data.Base.Repository;

namespace Proyecto.Distribuited.Services
{
    public class Startup
    {
        private const string MyCorsPolicyName = "CorsPolicy";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<FormOptions>(options =>
            {
                // Set the limit to 256 MB
                options.MultipartBodyLengthLimit = 268435456;
                options.MultipartHeadersLengthLimit = 268435456;
            });

            services.AddCors(o => o.AddPolicy(MyCorsPolicyName, builder =>
            {
                builder
                .WithOrigins("http://localhost:59489")
                .AllowAnyHeader()
                .AllowAnyMethod()
                .AllowCredentials()
                .SetIsOriginAllowed((host) => true);
            }));

            //Swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                { 
                    Title = "Sistema De Prueba Realplaza", 
                    Description = "API Web Realplaza",
                    Version = "v1" 
                });
                
                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        new string[] {}
                    }
                });
            });

            //DB Contexts
            services.AddDbContext<ProyectoContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.AddAutoMapper(typeof(Startup));

            //REGISTRAR TODOS LOS REPOSITORIOS - (CAPA INFRAESTRUCTURE.DATA)
            services.AddScoped<IProductsRepository, ProductsRepository>();


            //REGISTRAR LOS SERVICIOS DE APLICACION - (CAPA APPLICATION)
            services.AddScoped<IProductsAppService, ProductsAppService>();

            services.AddControllers();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "WEB API V1"));
            }

            app.UseRouting();
            app.UseCors(MyCorsPolicyName);
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
