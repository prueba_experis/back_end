﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Proyecto.Domain.Entities.Base.Models
{
    public partial class ProductModel
    {
        [Key]
        [Column("IDPRODUCT")]
        public int IDPRODUCT { get; set; }
        [Column("NAME")]
        public string? NAME { get; set; }
        [Column("PRICE")]
        public decimal? PRICE { get; set; }
    }
}
