﻿using System.Threading.Tasks;
using Proyecto.Application.Dto.Base.Products;


namespace Proyecto.Domain.Base
{
    public interface IProductsRepository
    {
        Task<ProductsResponseDto>  GetProductsAsync(ProductsRequestDto request);
    }
}
