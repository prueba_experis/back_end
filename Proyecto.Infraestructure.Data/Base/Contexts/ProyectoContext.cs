﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Proyecto.Domain.Entities.Base.Models;

namespace Proyecto.Infraestructure.Data.Base.Contexts
{
    public partial class ProyectoContext : DbContext
    {
        public IConfiguration Configuration { get; }
        public ProyectoContext()
        {

        }

        public ProyectoContext(DbContextOptions<ProyectoContext> options, IConfiguration configuration)
            : base(options)
        {
            Configuration = configuration;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
            }
        }


        #region TABLAS
        public virtual DbSet<ProductModel> ProductModel { get; set; }
        #endregion


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<ProductModel>(entity =>
            {
                entity.HasKey(e => e.IDPRODUCT);

                entity.ToTable("PRODUCT");

                entity.Property(e => e.NAME)
                    .HasMaxLength(160)
                    .IsUnicode(false);

                entity.Property(e => e.PRICE)
                    .IsUnicode(false);
            });

        }

    }
}