﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Proyecto.Application.Dto.Base.Products;
using Proyecto.Domain.Base;
using Proyecto.Domain.Entities.Base.Models;
using Proyecto.Infraestructure.Data.Base.Contexts;

namespace Proyecto.Infraestructure.Data.Base.Repository
{
    public class ProductsRepository : IProductsRepository
    {
        private readonly ProyectoContext _MContext;
        private readonly IMapper _mapper;

        public ProductsRepository(ProyectoContext dbc, IMapper mapper)
        {
            _MContext = dbc;
            _mapper = mapper;
        }

        public async Task<ProductsResponseDto> GetProductsAsync(ProductsRequestDto Request)
        {
            ProductsResponseDto result = new ProductsResponseDto();

            try
            {
                int cantPage = Request.dataPage.cantPage;
                List<ProductModel> resoltad = await _MContext.ProductModel
                    .Where(p => (p.NAME+" "+p.PRICE).Contains(Request.filter) )
                    .OrderBy(p => p.PRICE)
                    .Skip(Request.dataPage.numberPage * cantPage).Take(cantPage).ToListAsync();
                double total = (double)_MContext.ProductModel.Where(p => (p.NAME + " " + p.PRICE).Contains(Request.filter)).Count() / cantPage;
                result.maxPage = (int) Math.Ceiling(total);
               
                result.listData = _mapper.Map<List<ProductsDto>>(resoltad);
                result.success = 1;
                result.menssage = "success.";
            }
            catch (Exception ex)
            {
                result.success = 0;
                result.menssage = ex.Message;
            }
            return result;
        }

    }
}
